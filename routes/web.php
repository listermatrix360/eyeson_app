<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PageController@home')->name('home_page');
Auth::routes();

Route::prefix('dashboard')->group(function (){

    // Article Routes
    Route::resource('article', 'Admin\ArticleController');
    Route::get('article/delete/{id}', 'Admin\ArticleController@delete')->name('article.delete');
    Route::get('article/{id}/{title}', 'Admin\ArticleController@show');


    // Poem Routes
    Route::get('/poem/{id}/{title}', 'Admin\PoemController@show');
    Route::resource('poem', 'Admin\PoemController');
    Route::get('poem/delete/{id}', 'Admin\PoemController@delete')->name('poem.delete');



//    Route::get('/artwork/{id}/{title}', 'Admin\ArtworkController@show');
    Route::resource('artwork', 'Admin\ArtworkController');
    Route::get('artwork/delete/{id}', 'Admin\ArtworkController@delete')->name('poem.delete');


    Route::get('/', 'HomeController@index')->name('home');
    Route::prefix('videos')->group(function (){
        Route::get('index', 'HomeController@videos')->name('items.videos');
        Route::any('create', 'HomeController@videos_create')->name('items.videos.create');
        Route::any('edit/{video}', 'HomeController@videos_edit')->name('items.videos.edit');
        Route::any('delete/{video}', 'HomeController@videos_delete')->name('items.videos.delete');
    });

    Route::prefix('pdf')->group(function (){
        Route::get('index', 'PdfController@index')->name('items.document');
        Route::any('create', 'PdfController@create')->name('items.document.create');
        Route::any('edit/{video}', 'PdfController@edit')->name('items.document.edit');
        Route::any('delete/{video}', 'PdfController@delete')->name('items.document.delete');
    });

    Route::prefix('audio')->group(function (){
        Route::get('index', 'HomeController@videos')->name('items.audio');
        Route::any('create', 'HomeController@videos_create')->name('items.audio.create');
        Route::any('edit/{video}', 'HomeController@videos_edit')->name('items.audio.edit');
        Route::any('delete/{video}', 'HomeController@videos_delete')->name('items.audio.delete');
    });


    Route::prefix('advert')->group(function (){
        Route::get('index', 'HomeController@videos')->name('items.advert');
        Route::any('create', 'HomeController@videos_create')->name('items.advert.create');
        Route::any('edit/{video}', 'HomeController@videos_edit')->name('items.advert.edit');
        Route::any('delete/{video}', 'HomeController@videos_delete')->name('items.advert.delete');
    });


});


Route::get('member/signup', 'Auth\RegisterController@showMemberRegisterForm')->name('signup');

Route::post('member/register', 'Auth\RegisterController@createMember')->name('register');

Route::get('member/member_login', 'Auth\MemberLoginController@showMemberLoginForm')->name('member_login');

Route::post('member/login', 'Auth\MemberLoginController@MemberLogin')->name('login_member');



Route::group(['middleware' => ['auth:member']], function () {

// Portal Homepage
	Route::get('member/home', 'Portal\PortalPagesController@member_home')->name('dashboard');

	Route::resource('member/profile', 'Portal\ProfileController', ['names' => [
		'index' => 'member.profile.index',
		'edit' => 'member.profile.edit',
		'update' => 'member.profile.update',
	]]);

	Route::resource('tasks', 'TasksController', ['names' => [
		'index' => 'task.index',
		'edit' => 'task.edit',
		'update' => 'task.update',
	]]);
});

Route::view('forgot_password', 'subscriber..password_reset')->name('member.password_reset');

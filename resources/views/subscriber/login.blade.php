<!DOCTYPE html>
<html>
<head>
    <title>Watered Garden Church | Login</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900%7CLora:400,400i,700,700i" rel="stylesheet">

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

  <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('website/css/responsive.css')}}">
    <!-- Addition of Custom css file -->
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/custom.responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/all.min.css') }}">
    <script type="text/javascript" src="{{ asset('website/js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</head>
<body class="auth_bg">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="auth_form_section">
                        <figure class="">
                            <img class="" src="{{ asset('website/images/logo1.png')}}">
                        </figure>

                        <h6 class="text-center">Login to Your Portal</h6>
                        @if($errors->any() > 0)
                        @foreach($errors->all() as $error)
                        <div class=" alert-error">
                            {{ $error }}
                        </div>
                        @endforeach
                        @endif
                        <form method="POST" action="{{ route('login_member') }}">
                            @csrf
                            <div class="wrapper">
                              <input type="email" name="email" placeholder="{{ __('E-Mail Address') }}" required autocomplete="email" autofocus>
                          </div>
                          <div class="wrapper">
                            <input type="password" name="password" placeholder="{{ __('Password') }}" required autocomplete="current-password">
                        </div>
                        <div class="row">
                            @if (Route::has('password.request'))
                            <div class="col-md-6">
                              <small><a href="{{ route("member.password_reset") }}"> {{ __('Forgot Your Password?') }}</a></small>
                          </div>
                          @endif
                          <div class="col-md-6" align="right">
                              <small><a href="{{ route('signup') }}">Don't have an account?</a></small>
                          </div>
                      </div>
                      <p align="center"><button type="submit" class="btn btn-style-5">{{ __('Login') }}</button></p>
                      <p align="center">
                        <small>
                            <a href="{{ route('home_page') }}">Go Home</a>
                        </small>
                    </p>
                </form>
            </div>
        </div>
{{--                <div class="col-md-4">--}}
{{--                    <div class="card card-body">--}}
{{--            <h3 class="text-center">GET HELP</h3>--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-12">--}}
{{--                    <ul class="custom-list item-4">--}}
{{--                        <li>--}}
{{--                            <span>--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path class="heroicon-ui" d="M18.59 13H3a1 1 0 0 1 0-2h15.59l-5.3-5.3a1 1 0 1 1 1.42-1.4l7 7a1 1 0 0 1 0 1.4l-7 7a1 1 0 0 1-1.42-1.4l5.3-5.3z" style="fill:red;"/></svg>--}}
{{--                            </span>--}}
{{--                            <a href="{{ route('weddings') }}" class="text-danger">Weddings</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <span>--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path class="heroicon-ui" d="M18.59 13H3a1 1 0 0 1 0-2h15.59l-5.3-5.3a1 1 0 1 1 1.42-1.4l7 7a1 1 0 0 1 0 1.4l-7 7a1 1 0 0 1-1.42-1.4l5.3-5.3z" style="fill:red;"/></svg>--}}
{{--                            </span>--}}
{{--                            <a href="{{ route('bereavement') }}" class="text-danger">Bereavement</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <span>--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path class="heroicon-ui" d="M18.59 13H3a1 1 0 0 1 0-2h15.59l-5.3-5.3a1 1 0 1 1 1.42-1.4l7 7a1 1 0 0 1 0 1.4l-7 7a1 1 0 0 1-1.42-1.4l5.3-5.3z" style="fill:red;"/></svg>--}}
{{--                            </span>--}}
{{--                            <a href="{{ route('newborn') }}" class="text-danger">Welcoming a newborn</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--           </div>--}}
{{--               </div>--}}
            </div>
</div>
</div>
</body>
</html>

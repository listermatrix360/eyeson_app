@extends('layouts.web_master')
@section('content')
<section>
  <div class="pro-cover">
  </div>
  @include('web_pages.includes/portal_sidebar')
  <div class="stu-db">
    <div class="container-fluid pg-inn">
      <div class="row">
        @include('web_pages.includes/client_bio')
        <div class="col-md-9">
          <div class="udb">
            @if($daily > 0)
            <div class="udb-sec udb-prof">
              <h4> Today's Devotional Guide <span>({{ now()->format('l M d, Y') }})</span></h4>
              <p>
                {!! Str::limit($daily->message, 130) !!}<br><br>
                <a href="{{ $daily->devotion() }}" class="btn btn-style-5 r-round">Continue Reading</a>
              </p>
            </div>
            @endif
            <div class="udb-sec udb-cour">
              <h4 class="mb-5"> Latest News</h4>
              <div class="row">
                @foreach($posts as $post)
                <div class="col-lg-6 col-md-6">
                  <div class="blog-grid">
                    <div class="blog-grid-image">
                      <a href="#"><img src="../{{ $post->blog_image }}" alt=""></a>
                    </div>
                    <div class="blog-grid-text">
                      <h5 class=""><a href="{{$post->posts() }}">{{ $post->title }}</a></h5>
                      
                      <div class="hr-1 opacity-1 mt-10 mb-10"></div>
                      <section>{!! Str::limit($post->description, 230) !!}</section>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
            <div class="udb-sec udb-cour">
              <h4> Upcoming Events</h4>
              <div class="container event-cards-section">
                <div class="event-box style-2 list-type">
                  @foreach($events as $event)
                  <div class="event">
                    <div class="event-date">
                      <h3 class="numb">{{ date('d',strtotime($event->sdate)) }}</h3>
                      <h6 class="month">{{ date('M',strtotime($event->sdate)) }}</h6>
                      <div class="day">{{ date('l',strtotime($event->sdate)) }}</div>
                    </div>
                    <div class="event-img">              
                      <a href="#"><img src="{{ asset('website/images/m1.jpg') }}" alt=""></a>
                    </div>
                    <div class="event-body">
                      <h5 class="event-title"><a href="{{$event->postUrl() }}">{{ $event->title }}</a> {{-- <span class="price">Free</span> --}}</h5>
                      <p class="e-info">{{ date('d M l', strtotime($event->sdate)) }} @ {{ date('h:iA', strtotime($event->stime)) }} - {{ date('h:iA', strtotime($event->etime)) }} <br>
                       {{ $event->venue }}</p>
                       <p>{!! Str::limit($event->description, 200) !!}</p>
                       <div class="event-action flex-row align-items-center">
                        <a href="{{$event->postUrl() }}" class="btn btn-style-8">More Details</a>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--SECTION END-->

@endsection
{{-- @extends('layouts.web_master') --}}
<!DOCTYPE html>
<html>
<head>
    <title>Watered Garden Church | Sign Up</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900%7CLora:400,400i,700,700i" rel="stylesheet">

<!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

  <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/custom.css') }}">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</head>
<body class="auth_bg">
<div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="auth_form_section">
                    <figure class="">
                        <img class="" src="{{ asset('website/images/logo1.png')}}">
                    </figure>
                    <h6 class="text-center">Create a Portal Accout</h6>
                    @if($errors->any() > 0)
                    @foreach($errors->all() as $error)
    <div class=" alert-error">
        {{ $error }}
                </div>
                @endforeach
    @endif
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="wrapper">
                          <input type="text" name="name" placeholder="{{ __('Name') }}" required autocomplete="name" autofocus value="{{ old('name') }}">
                      </div>
                      <div class="wrapper">
                          <input type="email" name="email" placeholder="{{ __('E-Mail Address') }}" required autocomplete="email" autofocus>
                      </div>
                      <div class="wrapper">
                        <input type="password" name="password" placeholder="{{ __('Password') }}" required autocomplete="new-passwor">
                      </div>
                      <div class="wrapper">
                        <input type="password" name="password_confirmation" id="password-confirm" placeholder="{{ __('Confirm Password') }}" required autocomplete="current-password">
                      </div>
                      <div class="row">
                           <div class="col-md-12" align="right">
                              <small><a href="{{ route('member_login') }}">Already have an account?</a></small>
                          </div>
                      </div>
                      <p align="center"><button type="submit" class="btn btn-style-5">{{ __('Register') }}</button></p>
                      <p align="center">
                        <small>
                            <a href="{{ route('home_page') }}">Go Home</a>
                        </small>
                    </p>
                  </form>
              </div>
          </div>
          <div class="col-md-8"></div>
      </div>
  </div>
</div>

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Zedek</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/iconfonts.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('css/colorbox.css')}}">
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>

</head>

<body>
<div class="trending-bar trending-light d-md-block">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-9 text-center text-md-left">
                <p class="trending-title"><i class="tsicon fa fa-bolt"></i> Trending Now</p>
                <div id="trending-slide" class="owl-carousel owl-theme trending-slide">
                    <div class="item">
                        <div class="post-content">
                            <h2 class="post-title title-small">
                                <a href="#">The best MacBook Pro alternatives in 2017 for Apple users</a>
                            </h2>
                        </div><!-- Post content end -->
                    </div><!-- Item 1 end -->
                    <div class="item">
                        <div class="post-content">
                            <h2 class="post-title title-small">
                                <a href="#">Soaring through Southern Patagonia with the Premium Byrd drone</a>
                            </h2>
                        </div><!-- Post content end -->
                    </div><!-- Item 2 end -->
                    <div class="item">
                        <div class="post-content">
                            <h2 class="post-title title-small">
                                <a href="#">Super Tario Run isn’t groundbreaking, but it has Mintendo charm</a>
                            </h2>
                        </div><!-- Post content end -->
                    </div><!-- Item 3 end -->
                </div><!-- Carousel end -->
            </div><!-- Col end -->
            <div class="col-md-3 text-md-right text-center">
                <div class="ts-date">
                    <i class="fa fa-calendar-check-o"></i>May 29, 2017
                </div>
            </div><!-- Col end -->
        </div><!--/ Row end -->
    </div><!--/ Container end -->
</div><!--/ Trending end -->

<!-- Header start -->
<header id="header" class="header">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-md-3 col-sm-12">
                <div class="logo">
                    <a href="index.html">
                        <img src="{{asset('images/zedek.jpg')}}" alt="" style="height: 100px; width: 100px;">
                    </a>
                </div>
            </div><!-- logo col end -->

            <div class="col-md-8 col-sm-12 header-right">
                <div class="ad-banner float-right">
                    <a href="#">
                        <img src="images/banner-image/image1.png" class="img-fluid" alt="">
                    </a>
                </div>
            </div><!-- header right end -->
        </div><!-- Row end -->
    </div><!-- Logo and banner area end -->
</header><!--/ Header end -->

<div class="main-nav clearfix is-ts-sticky">
    <div class="container">
        <div class="row justify-content-between">
            <nav class="navbar navbar-expand-lg col-lg-8">
                <div class="site-nav-inner float-left">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
                        <span class="fa fa-bars"></span>
                    </button>
                    <!-- End of Navbar toggler -->
                    <div id="navbarSupportedContent" class="collapse navbar-collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav">
                            <li class="nav-item dropdown active">
                                <a href="#" class="menu-dropdown" data-toggle="dropdown">Home <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="active"><a href="{{route('home_page')}}">Welcome</a></li>
                                </ul>
                            </li>


                            <li class="dropdown nav-item mega-dropdown">
                                <a href="#" class="dropdown-toggler menu-dropdown" data-toggle="dropdown">INTERVIEW <i class="fa fa-angle-down"></i></a>
                                <!-- responsive dropdown -->
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="category.html">Category 1</a></li>
                                    <li><a href="category-2.html">Category 2</a></li>
                                    <li><a href="category.html">Category 3</a></li>
                                </ul>
                                <!-- responsive dropdown end -->
                                <div class="dropdown-menu mega-menu-content clearfix">
                                    <div class="mega-menu-content-inner">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="post-block-style clearfix">
                                                    <div class="post-thumb">
                                                        <img class="img-fluid" src="images/news/video/video4.jpg" alt="" />
                                                        <a class="popup" href="https://www.youtube.com/embed/XhveHKJWnOQ?autoplay=1&amp;loop=1">
                                                            <div class="video-icon">
                                                                <i class="fa fa-play"></i>
                                                            </div>
                                                        </a>
                                                    </div><!-- Post thumb end -->
                                                    <div class="post-content">
                                                        <h2 class="post-title title-small">
                                                            <a href="#">Netcix cuts out the chill with an integrated...</a>
                                                        </h2>
                                                    </div><!-- Post content end -->
                                                </div><!-- Post Block style end -->
                                            </div><!-- Col 1 end -->

                                            <div class="col-md-3">
                                                <div class="post-block-style clearfix">
                                                    <div class="post-thumb">
                                                        <img class="img-fluid" src="images/news/video/video3.jpg" alt="" />
                                                        <a class="popup" href="https://www.youtube.com/embed/wJF5NXygL4k?autoplay=1&amp;loop=1">
                                                            <div class="video-icon">
                                                                <i class="fa fa-play"></i>
                                                            </div>
                                                        </a>
                                                    </div><!-- Post thumb end -->
                                                    <div class="post-content">
                                                        <h2 class="post-title title-small">
                                                            <a href="#">Netcix cuts out the chill with an integrated...</a>
                                                        </h2>
                                                    </div><!-- Post content end -->
                                                </div><!-- Post Block style end -->
                                            </div><!-- Col 2 end -->

                                            <div class="col-md-3">
                                                <div class="post-block-style clearfix">
                                                    <div class="post-thumb">
                                                        <img class="img-fluid" src="images/news/video/video2.jpg" alt="" />
                                                        <a class="popup" href="https://www.youtube.com/embed/DQNDcxRo-2M?autoplay=1&amp;loop=1">
                                                            <div class="video-icon">
                                                                <i class="fa fa-play"></i>
                                                            </div>
                                                        </a>
                                                    </div><!-- Post thumb end -->
                                                    <div class="post-content">
                                                        <h2 class="post-title title-small">
                                                            <a href="#">TG G6 will have dual 13-megapixel cameras...</a>
                                                        </h2>
                                                    </div><!-- Post content end -->
                                                </div><!-- Post Block style end -->
                                            </div><!-- Col 3 end -->
                                            <div class="col-md-3">
                                                <div class="post-block-style clearfix">
                                                    <div class="post-thumb">
                                                        <img class="img-fluid" src="images/news/video/video1.jpg" alt="" />
                                                        <a class="popup" href="https://www.youtube.com/embed/DQNDcxRo-2M?autoplay=1&amp;loop=1">
                                                            <div class="video-icon">
                                                                <i class="fa fa-play"></i>
                                                            </div>
                                                        </a>
                                                    </div><!-- Post thumb end -->
                                                    <div class="post-content">
                                                        <h2 class="post-title title-small">
                                                            <a href="#">Netcix cuts out the chill with an integrated...</a>
                                                        </h2>
                                                    </div><!-- Post content end -->
                                                </div><!-- Post Block style end -->
                                            </div><!-- Col 4 end -->
                                        </div><!-- Post block row end -->
                                    </div>
                                </div><!-- Mega menu content end -->
                            </li><!-- Video menu end -->

                            <li class="nav-item dropdown active">
                                <a href="#" class="menu-dropdown" data-toggle="dropdown">RESOURCES <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="active"><a href="{{route('home_page')}}">PDF</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown active">
                                <a href="#" class="menu-dropdown" data-toggle="dropdown">ACCOUNT <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="active"><a href="{{route('home_page')}}">LOGIN</a></li>
                                    <li><a href="#">SIGN UP</a></li>
                                </ul>
                            </li>
                        </ul><!--/ Nav ul end -->
                    </div><!--/ Collapse end -->

                </div><!-- Site Navbar inner end -->
            </nav><!--/ Navigation end -->

            <div class="col-lg-4 text-right nav-social-wrap">
                <div class="top-social">
                    <ul class="social list-unstyled">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>


                <div class="nav-search">
                    <a href="#search-popup" class="xs-modal-popup">
                        <i class="icon icon-search1"></i>
                    </a>
                </div><!-- Search end -->

                <div class="zoom-anim-dialog mfp-hide modal-searchPanel ts-search-form" id="search-popup">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="xs-search-panel">
                                <form class="ts-search-group">
                                    <div class="input-group">
                                        <input type="search" class="form-control" name="s" placeholder="Search" value="">
                                        <button class="input-group-btn search-button">
                                            <i class="icon icon-search1"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- End xs modal -->
            </div>
        </div><!--/ Row end -->
    </div><!--/ Container end -->
</div><!-- Menu wrapper end -->

<div class="gap-30"></div>

<section class="trending-slider pt-lg-0">
    <div class="container">
        <div class="ts-grid-box">
            <div class="owl-carousel dot-style2 transing-slide-style2">
                <div class="item post-overaly-style post-lg" style="background-image:url(images/news/fashion/image1.png)">
                    <a href="#" class="image-link">&nbsp;</a>
                    <div class="overlay-post-content">
                        <div class="post-content">
                            <div class="grid-category">
                                <a class="post-cat fashion" href="#">Fashion</a>
                            </div>

                            <h2 class="post-title title-md">
                                <a href="#">The loss is not all that surprising given</a>
                            </h2>
                            <div class="post-meta">
                                <ul>
                                    <li><a href="#"><i class="fa fa-user"></i> John Wick</a></li>
                                    <li><a href="#"><i class="fa fa-clock-o"></i> 20 July, 2020</a></li>
                                    <li><a href="#" class="view"><i class="icon icon-fire"></i> 354k</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- Item 1 end -->
                <div class="item post-overaly-style post-lg" style="background-image:url(images/news/travel/image1.png)">
                    <a href="#" class="image-link">&nbsp;</a>
                    <div class="overlay-post-content">
                        <div class="post-content">
                            <div class="grid-category">
                                <a class="post-cat lifestyle" href="#">Lifestyle</a>
                                <a class="post-cat sports" href="#">Sports</a>
                            </div>

                            <h2 class="post-title title-md">
                                <a href="#">The loss is not all that surprising given</a>
                            </h2>
                            <div class="post-meta">
                                <ul>
                                    <li><a href="#"><i class="fa fa-user"></i> John Wick</a></li>
                                    <li><a href="#"><i class="fa fa-clock-o"></i> 20 July, 2020</a></li>
                                    <li><a href="#" class="view"><i class="icon icon-fire"></i> 354k</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- Item 1 end -->
                <div class="item post-overaly-style post-lg" style="background-image:url(images/news/lifestyle/image2.png)">
                    <a href="#" class="image-link">&nbsp;</a>
                    <div class="overlay-post-content">
                        <div class="post-content">
                            <div class="grid-category">
                                <a class="post-cat fashion" href="#">Fashion</a>
                            </div>

                            <h2 class="post-title title-md">
                                <a href="#">The loss is not all that surprising given</a>
                            </h2>
                            <div class="post-meta">
                                <ul>
                                    <li><a href="#"><i class="fa fa-user"></i> John Wick</a></li>
                                    <li><a href="#"><i class="fa fa-clock-o"></i> 20 July, 2020</a></li>
                                    <li><a href="#" class="view"><i class="icon icon-fire"></i> 354k</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- Item 1 end -->
                <div class="item post-overaly-style post-lg" style="background-image:url(images/news/lifestyle/image3.png)">
                    <a href="#" class="image-link">&nbsp;</a>
                    <div class="overlay-post-content">
                        <div class="post-content">
                            <div class="grid-category">
                                <a class="post-cat travel" href="#">Travel</a>
                            </div>

                            <h2 class="post-title title-md">
                                <a href="#">The loss is not all that surprising given</a>
                            </h2>
                            <div class="post-meta">
                                <ul>
                                    <li><a href="#"><i class="fa fa-user"></i> John Wick</a></li>
                                    <li><a href="#"><i class="fa fa-clock-o"></i> 20 July, 2020</a></li>
                                    <li><a href="#" class="view"><i class="icon icon-fire"></i> 354k</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- Item 1 end -->
            </div>
            <!-- most-populers end-->
        </div>
        <!-- ts-populer-post-box end-->
    </div>
    <!-- container end-->
</section>

<section class="block pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="block-title">
                    <span class="title-angle-shap"> Weekend Top</span>
                </h2>
            </div>
            <div class="col-md-3">
                <div class="post-block-style">
                    <div class="post-thumb">
                        <a href="#">
                            <img class="img-fluid" src="images/news/fashion/image1.png" alt="">
                        </a>
                        <div class="grid-cat">
                            <a class="post-cat tech" href="#">Tech</a>
                        </div>
                    </div>

                    <div class="post-content">
                        <h2 class="post-title">
                            <a href="#">Ratcliffe to be Director of nation talent</a>
                        </h2>
                        <div class="post-meta mb-7">
                            <span class="post-author"><a href="#"><i class="fa fa-user"></i> John Doe</a></span>
                            <span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
                        </div>
                    </div><!-- Post content end -->
                </div><!-- post-block -->
            </div><!-- col end -->
            <div class="col-md-3">
                <div class="post-block-style">
                    <div class="post-thumb">
                        <a href="#">
                            <img class="img-fluid" src="images/news/health/image1.png" alt="">
                        </a>
                        <div class="grid-cat">
                            <a class="post-cat tech" href="#">Tech</a>
                        </div>
                    </div>

                    <div class="post-content">
                        <h2 class="post-title">
                            <a href="#">Zhang social media pop star also known innocent</a>
                        </h2>
                        <div class="post-meta mb-7">
                            <span class="post-author"><a href="#"><i class="fa fa-user"></i> John Doe</a></span>
                            <span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
                        </div>
                    </div><!-- Post content end -->
                </div><!-- post-block -->
            </div><!-- col end -->
            <div class="col-md-3">
                <div class="post-block-style">
                    <div class="post-thumb">
                        <a href="#">
                            <img class="img-fluid" src="images/news/tech/tech02.png" alt="">
                        </a>
                        <div class="grid-cat">
                            <a class="post-cat tech" href="#">Tech</a>
                        </div>
                    </div>

                    <div class="post-content">
                        <h2 class="post-title">
                            <a href="#">Zhang social media pop star also known innocent</a>
                        </h2>
                        <div class="post-meta mb-7">
                            <span class="post-author"><a href="#"><i class="fa fa-user"></i> John Doe</a></span>
                            <span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
                        </div>
                    </div><!-- Post content end -->
                </div><!-- post-block -->
            </div><!-- col end -->
            <div class="col-md-3">
                <div class="post-block-style">
                    <div class="post-thumb">
                        <a href="#">
                            <img class="img-fluid" src="images/news/health/image2.png" alt="">
                        </a>
                        <div class="grid-cat">
                            <a class="post-cat tech" href="#">Tech</a>
                        </div>
                    </div>

                    <div class="post-content">
                        <h2 class="post-title">
                            <a href="#">Zhang social media pop star also known innocent</a>
                        </h2>
                        <div class="post-meta mb-7">
                            <span class="post-author"><a href="#"><i class="fa fa-user"></i> John Doe</a></span>
                            <span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
                        </div>
                    </div><!-- Post content end -->
                </div><!-- post-block -->
            </div><!-- col end -->
        </div><!-- row end -->
    </div>
</section>





<!-- ad banner start-->
<div class="block-wrapper no-padding">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="banner-img">
                    <a href="index.html">
                        <img class="img-fluid" src="images/banner-image/image4.png" alt="">
                    </a>
                </div>
            </div>
            <!-- col end -->
        </div>
        <!-- row  end -->
    </div>
    <!-- container end -->
</div>
<!-- ad banner end-->
<div class="gap-50"></div>
<!-- ad banner start-->

<div class="newsletter-area">
    <div class="container">
        <div class="row ts-gutter-30 justify-content-center align-items-center">
            <div class="col-lg-7 col-md-6">
                <div class="footer-loto">
                    <a href="#">
                        <img src="images/logos/logo-light.png" alt="">
                    </a>
                </div>
            </div>
            <!-- col end -->
            <div class="col-lg-5 col-md-6">
                <div class="footer-newsletter">
                    <form action="#" method="post">
                        <div class="email-form-group">
                            <i class="news-icon fa fa-paper-plane" aria-hidden="true"></i>
                            <input type="email" name="EMAIL" class="newsletter-email" placeholder="Your email" required>
                            <input type="submit" class="newsletter-submit" value="Subscribe">
                        </div>

                    </form>
                </div>
            </div>
            <!-- col end -->
        </div>
        <!-- row  end -->
    </div>
    <!-- container end -->
</div>
<!-- ad banner end-->

<!-- Footer start -->
<div class="ts-footer">
    <div class="container">
        <div class="row ts-gutter-30 justify-content-lg-between justify-content-center">
            <div class="col-lg-4 col-md-6">
                <div class="footer-widtet">
                    <h3 class="widget-title"><span>About Us</span></h3>
                    <div class="widget-content">
                        <p>Hidden Hills property with mountain and city view boast nine bedrooms including a master suite with private terrace and an entertainment. wing which includes a 20-seat theater.</p>
                        <ul class="ts-footer-info">
                            <li><i class="fa fa-home"></i> 15 Cliff St, New York NY 10038, USA</li>
                            <li><i class="icon icon-phone2"></i> +1 212-602-9641</li>
                            <li><i class="fa fa-envelope"></i>info@example.com</li>
                        </ul>
                        <ul class="ts-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- col end -->
            <div class="col-lg-3 col-md-6">
                <div class="footer-widtet post-widget">
                    <h3 class="widget-title"><span>Popular Post</span></h3>
                    <div class="widget-content">
                        <div class="list-post-block">
                            <ul class="list-post">
                                <li>
                                    <div class="post-block-style media">
                                        <div class="post-thumb">
                                            <a href="#">
                                                <img class="img-fluid" src="images/news/tech/image5.png" alt="">
                                            </a>
                                        </div><!-- Post thumb end -->

                                        <div class="post-content media-body">
                                            <h4 class="post-title">
                                                <a href="#">Santino loganne legan an  old resident</a>
                                            </h4>
                                            <div class="post-meta mb-7">
                                                <span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
                                            </div>
                                        </div><!-- Post content end -->
                                    </div><!-- Post block style end -->
                                </li><!-- Li 1 end -->
                                <li>
                                    <div class="post-block-style media">
                                        <div class="post-thumb">
                                            <a href="#">
                                                <img class="img-fluid" src="images/news/fashion/image4.png" alt="">
                                            </a>
                                        </div><!-- Post thumb end -->

                                        <div class="post-content media-body">
                                            <h4 class="post-title">
                                                <a href="#">Jennifer Lopez expan her property</a>
                                            </h4>
                                            <div class="post-meta mb-7">
                                                <span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
                                            </div>
                                        </div><!-- Post content end -->
                                    </div><!-- Post block style end -->
                                </li><!-- Li 2 end -->
                                <li>
                                    <div class="post-block-style media">
                                        <div class="post-thumb">
                                            <a href="#">
                                                <img class="img-fluid" src="images/news/tech/tech1.png" alt="">
                                            </a>
                                        </div><!-- Post thumb end -->

                                        <div class="post-content media-body">
                                            <h4 class="post-title">
                                                <a href="#">Zhang social media pop star also known</a>
                                            </h4>
                                            <div class="post-meta mb-7">
                                                <span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
                                            </div>
                                        </div><!-- Post content end -->
                                    </div><!-- Post block style end -->
                                </li><!-- Li 3 end -->
                                <li>
                                    <div class="post-block-style media">
                                        <div class="post-thumb">
                                            <a href="#">
                                                <img class="img-fluid" src="images/news/fashion/image1.png" alt="">
                                            </a>
                                        </div><!-- Post thumb end -->

                                        <div class="post-content media-body">
                                            <h4 class="post-title">
                                                <a href="#">House last week that move would Inject</a>
                                            </h4>
                                            <div class="post-meta mb-7">
                                                <span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
                                            </div>
                                        </div><!-- Post content end -->
                                    </div><!-- Post block style end -->
                                </li><!-- Li 3 end -->
                            </ul><!-- list-post end -->
                        </div>
                    </div>
                </div>
            </div><!-- col end -->
            <div class="col-lg-3 col-md-6">
                <div class="footer-widtet post-widget">
                    <div class="widget-content">
                        <div class="footer-ads">
                            <a href="#">
                                <img class="img-fluid" src="images/banner-image/image6.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- container end -->
</div>

<div class="ts-copyright">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-12 text-center">
                <div class="copyright-content text-light">
                    <p>&copy; 2019, Digiqole - News Magazine html Template. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="top-up-btn">
    <div class="backto" style="display: block;">
        <a href="#" class="icon icon-arrow-up" aria-hidden="true"></a>
    </div>
</div>

<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.colorbox.js"></script>
<script src="js/custom.js"></script>
</body>

</html>

@extends('layouts.app')
@include('admin.header')

@section('content')
    <div class="page-content">
        <!-- Sidebar -->
    @include('layouts.sidebar')
    <!-- Main content -->
        <div class="content-wrapper mt-5">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">articleS</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="{{ route('home')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <a href="" class="breadcrumb-item">View articles</a>

                    </div>
                </div>
            </div>
            <div class="content">
               @include('layouts.session')
            <!-- Inner container -->
                <div class="d-flex align-items-start flex-column flex-md-row">
                    @if(count($articles) > 0)
                        @foreach($articles as $article)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-img-actions mb-3">
                                            <img class="card-img img-fluid" src="{{ asset($article->image) }}" alt="">
                                            <div class="card-img-actions-overlay card-img">
                                                <a href="" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
                                                    <i class="icon-link"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <h5 class="font-weight-semibold mb-1">
                                            <a href="" class="text-default">{{ $article->title }}</a>
                                        </h5>

                                        <ul class="list-inline list-inline-dotted text-muted mb-3">
                                            <li class="list-inline-item">{{ date('M d, Y', strtotime($article->created_at)) }}</li>
                                        </ul>
                                        <section>
                                            {!! Str::limit($article->content, 200) !!}
                                        </section>
                                    </div>
                                    <br>
                                    <div class="card-footer d-flex">
                                        <a href="{{ route('article.edit', $article->id) }}" class="btn btn-primary mr-2"><span class="fa fa-edit"></span></a>
                                        <a href="{{ route('article.delete', $article->id) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this article?')"><span class="fa fa-trash"></span></a>
                                        <a href="{{ $article->articleUrl() }}" class="ml-auto">Read more <i class="icon-arrow-right14 ml-2"></i></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <h1>There are no new articles at the moment</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

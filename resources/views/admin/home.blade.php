@extends('layouts.app')
@include('admin.header')

@section('content')
<!-- Page content -->
<div class="page-content">
    <!-- Sidebar -->
    @include('layouts.sidebar')
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Content area -->
        <div class="content mt-5">
            <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-3">
                    <div class="card card-body bg-indigo">
                        <div class="d-flex w-100">
                            <p> Users <br> <span class="ml-5" style="font-size: 40px;"><strong >{{ $members }}</strong></span></p>
                            <i class="icon-users ml-5 display-4"></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-3">
                    <div class="card card-body bg-danger">
                        <div class="d-flex w-100">
                            <p>Resources <br><span class="ml-5" style="font-size: 40px;"><strong >{{ $an }}</strong></span></p>
                            <i class="fa fa-bullhorn ml-5 display-4"></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-3">
                    <div class="card card-body bg-success">
                        <div class="d-flex w-100">
                            <p>Revenue <br><span class="ml-5" style="font-size: 40px;"><strong >{{ $event }}</strong></span></p>
                            <i class="fa fa-calendar ml-5 display-4"></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-3">
                    <div class="card card-body bg-dark">
                        <div class="d-flex w-100">
                            <p> Products <br> <span class="ml-5" style="font-size: 40px;"><strong >{{ $product }}</strong></span></p>
                            <i class="fab fa-opencart ml-5 display-4"></i>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /content area -->
@include('layouts.footer')
@endsection


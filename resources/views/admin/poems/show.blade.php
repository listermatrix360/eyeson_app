@extends('layouts.app')
@include('admin.header')

@section('content')
    <div class="page-content">
    @include('layouts.sidebar')
        <!-- Sidebar -->
        <div class="content-wrapper mt-5">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">POEMS</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="{{ route('home')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <a href="{{ route('poem.index') }}" class="breadcrumb-item">Poems</a>
                        <a href="" class="breadcrumb-item">{{$poem->title}}</a>
                    </div>
                </div>
            </div>
            <section class="blogs pt-5 pl-4">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="posts">
                                <div class="post">
                                    <div class="post-img">
                                        <a href="javascript:void(0);" class="width-100 ">
                                            <img src="{{ asset($poem->image) }}" alt="">
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="blog-list-simple-text">
                                            <div class="post-title">
                                                <h5 class="mb-3 ml-0">{{$poem->title}}</h5>
                                            </div>
                                            <ul class="meta">
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <i aria-hidden="true" class="fa fa-calendar-alt"></i> {{ date('l M d, Y', strtotime($poem->created_at)) }}
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <i aria-hidden="true" class="fa fa-clock"></i> {{ date('h:iA', strtotime($poem->created_at)) }}
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="post-cont">
                                            {!! $poem->content !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@extends('layouts.app')
@include('admin.header')

@section('content')
    <div class="page-content">
        <!-- Sidebar -->
    @include('layouts.sidebar')
    <!-- Main content -->
        <div class="content-wrapper mt-5">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">ARTWORK</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="{{ route('home')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <a href="" class="breadcrumb-item">Add Artwork</a>

                    </div>
                </div>
            </div>
            <div class="content">
                <!-- 2 columns form -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5></h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @include('layouts.session')
                        <form class="row" method="POST" action="{{ route('artwork.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col-md-6">
                                <label>Title:</label>
                                <input name="file" type="file"  class="form-control"  value="">
                                @error('file')
                                <p class="text-danger">{{ $errors->first('file')}}</p>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label>Artwork Thumbnail:</label>
                                <input name="image" class="form-control" type="file" placeholder=""  value="">
                                @error('image')
                                <p class="text-danger">{{ $errors->first('image')}}</p>
                                @enderror
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary mt-3">Add Artwork</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!DOCTYPE html>
<html>
<head>
    <title>Watered Garden Church | Login</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900%7CLora:400,400i,700,700i" rel="stylesheet">

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

  <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/custom.css') }}">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</head>
<body class="auth_bg">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="auth_form_section">
                        <figure class="">
{{--                            <img class="" src="{{ asset('website/images/logo1.png')}}">--}}
                        </figure>

                        <h6 class="text-center">Admin Login Portal</h6>
                        @if($errors->any() > 0)
                        @foreach($errors->all() as $error)
                        <div class=" alert-error">
                            {{ $error }}
                        </div>
                        @endforeach
                        @endif
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="wrapper">
                              <input type="email" name="email" class="@error('email') is-invalid @enderror"  placeholder="{{ __('E-Mail Address') }}" value="{{ old('email') }}" required autocomplete="email" autofocus>
                              @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>
                          <div class="wrapper">
                            <input type="password" name="password" class="@error('password') is-invalid @enderror" placeholder="{{ __('Password') }}" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row">
                            @if (Route::has('password.request'))
                            <div class="col-md-6">
                              <small><a href="{{ route('password.request') }}"> {{ __('Forgot Your Password?') }}</a></small>
                          </div>
                          @endif
                      </div>
                      <p align="center">
                          <button type="submit" class="btn btn-style-5">{{ __('Login') }}</button></p>
                </form>
            </div>
        </div>
        <div class="col-md-8"></div>
    </div>
</div>
</div>
</body>
</html>

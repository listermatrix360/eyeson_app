<div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text" style="text-align: center">
                        &copy; Copyright 2020. All Rights Reserved.
                    </span>
                </div>
            </div>
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</body>
</html>        <!-- /footer -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->
</div>
<script>
    $("#phone").keydown((e)=>{
        if(e.which > 64 && e.which < 91)
        {
        e.preventDefault();
        alert("Enter a Valid Phone Number");
        }
        });
    $('#phone').on('blur', function(e) {
        const regex = /^\D*(\d{3})\D*(\d{3})\D*(\d{4})\D*$/g;
        const number =  $(this).val().replace(regex,  '($1) $2-$3');
        $('#phone').val(number);
     });
     
</script>


@if(Session::has('success'))
    <p class="alert alert-success">{{Session('success')}}</p>
@endif
@if(Session::has('fail'))
    <p class="alert alert-danger">{{Session('fail')}}</p>
    @endif

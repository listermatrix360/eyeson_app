<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Zedek</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    {{--  DATATABLE CSS --}}
     <link href="{{asset('css/datatable/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatable/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatable/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatable/buttons.dataTables.min.css')}}" rel="stylesheet">



    <!-- DATATABLE JS -->


    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/css/admin-custom.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('website/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('global_assets/css/summernote.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('global_assets/css/tagsinput.css') }}">
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{ asset('global_assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/ui/perfect_scrollbar.min.js') }}"></script>
    <script src="{{ asset('website/js/all.min.js') }}"></script>

    <!-- Theme JS files -->
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>

    <!-- Theme JS files -->
  {{--   <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script> --}}
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <!-- Theme JS files -->
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/dashboard.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/layout_fixed_sidebar_custom.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/charts/echarts/bars_tornados.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/charts/echarts/pies_donuts.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('global_assets/js/summernote.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/widgets_content.js') }}"></script>
    <script type="text/javascript" src="{{ asset('global_assets/js/tagsinput.js') }}"></script>
    <!-- /theme JS files -->
</head>
<body>
    <div id="app">
        <!-- Main navbar -->
    <!-- /main navbar -->
        <main class="mb-0">
            @yield('content')
        </main>
    </div>
      <script src="{{asset('js/datatable/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/datatable/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('js/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/datatable/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/datatable/buttons.server-side.js')}}"></script>
    @stack('tables')
    @stack('script')
    <script>
        $('#basic-datatables').DataTable({
        });
        CKEDITOR.replace('textbody');
        // CKEDITOR.replace('editbody');
        CKEDITOR.config.height ='300px';
    </script>
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>

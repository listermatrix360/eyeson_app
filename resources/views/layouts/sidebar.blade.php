<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md" >

    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>


    <!-- Sidebar content -->
    <div class="sidebar-content mt-5">
        <!-- User menu -->
        <div class="sidebar-user pt-5">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        @if (Auth::user()->avatar)
                        <img src="{{ URL::to('/') }}/ProfileImg/{{ Auth::user()->avatar }}" alt="Avatar" width="50" height="50" class="rounded-circle mt-5">
                        @else
                        <img src="{{ asset('/images/img/user.png') }}" alt="Avatar"  width="50" height="50" class="rounded-circle mt-5">
                        @endif
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold">{{ Auth::user()->name }}</div>
                        <div class="media-title font-weight-semibold">{{ Auth::user()->email }}</div>
                    </div>




                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item">
                    <a href="{{ route('home')}}" class="nav-link {{ Request::path() === 'dashboard' ? 'active' : ''}}">
                        <i class="icon-home4"></i>
                        <span>
                            Home
                        </span>
                    </a>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="" class="nav-link"><i class="fab fa-opencart mr-3" style="font-size: 18px;"></i> <span>Poems</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="{{route('poem.create')}}" class="nav-link active">Add Poem</a></li>
                        <li class="nav-item"><a href="{{ route('poem.index') }}" class="nav-link active">View Poems</a></li>

                    </ul>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="" class="nav-link"><i class="fab fa-opencart mr-3" style="font-size: 18px;"></i> <span>Articles</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="{{route('article.create')}}" class="nav-link active">Add Articles</a></li>
                        <li class="nav-item"><a href="{{ route('article.index') }}" class="nav-link active">View Articles</a></li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="" class="nav-link"><i class="fab fa-opencart mr-3" style="font-size: 18px;"></i> <span>Artwork</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="{{route('artwork.create')}}" class="nav-link active">Add Artwork</a></li>
                        <li class="nav-item"><a href="{{ route('artwork.index') }}" class="nav-link active">View Artworks</a></li>
                    </ul>
                </li>



                <li class="nav-item">
                    <a href="{{ route('items.videos')}}" class="nav-link {{ Request::path() === 'dashboard/videos/index' ? 'active' : ''}}">
                        <i class="icon-home4"></i>
                        <span>Videos</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('items.document')}}" class="nav-link {{ Request::path() === 'dashboard/document/index' ? 'active' : ''}}">
                        <i class="icon-home4"></i>
                        <span>Audio</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('items.document')}}" class="nav-link {{ Request::path() === 'dashboard/videos/index' ? 'active' : ''}}">
                        <i class="icon-home4"></i>
                        <span>Document/PDF</span>
                    </a>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link {{ Request::path() === 'staff' ? 'active' : ''}}"><i class="icon-users"></i> <span>Users</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="#" class="nav-link active">List of Users</a></li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link {{ Request::path() === 'sermons' ? 'active' : ''}}"><i class="fab fa-teamspeak mr-3" style="font-size: 18px;"></i> <span>Sermon</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="" class="nav-link active">Upload Sermon</a></li>
                    </ul>
                </li>









                <!-- /main -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Online Store</div> <i class="icon-menu" title="Forms"></i></li>
                <li class="nav-item">
                    <a href="" class="nav-link {{ Request::path() === 'product_categories' ? 'active' : ''}}">
                        <i class="fas fa-shopping-cart mr-3" style="font-size: 18px;"></i>
                        <span>
                            Resource Categories
                        </span>
                    </a>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="" class="nav-link"><i class="fab fa-opencart mr-3" style="font-size: 18px;"></i> <span>Resources</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="" class="nav-link active">Add Resource</a></li>
                        <li class="nav-item"><a href="{" class="nav-link active">View Resources</a></li>

                    </ul>
                </li>








                <li class="nav-item nav-item-submenu">
                    <a href="" class="nav-link"><i class="fa fa-newspaper mr-3" style="font-size: 18px;"></i> <span>Blog</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="" class="nav-link active">Add Blog Post</a></li>
                        <li class="nav-item"><a href="" class="nav-link active">View Blog Posts</a></li>

                    </ul>
                </li>


                <!-- /page kits -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Extras</div> <i class="icon-menu" title="Forms"></i></li>
                <li class="nav-item">
                    <a href="" class="nav-link"><i class="fa fa-box mr-3" style="font-size: 18px;"></i> <span>Activity Log</span></a>
                </li>
                <!-- /page kits -->

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'avatar' => '',
            'name' => 'Karikari',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
        ]);
//        // factory(\App\Devotion::class, 20)->create();
//        factory(\App\Products::class, 20)->create();
//        // factory(\App\Testimonies::class, 20)->create();
//        factory(\App\Member::class, 10)->create();
//        factory(\App\Categories::class, 10)->create();
    }
}


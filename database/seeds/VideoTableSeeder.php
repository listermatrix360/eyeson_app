<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $videos = DB::table('videos');
        $videos->truncate();
        $videos->insert(
            [
                [
                'video_link' => 'https://www.youtube.com/embed/iyu6G6ukpbA',
                'video_description' => 'THE HOLY SPIRIT IN A BODILY FORM BY PROPHET MANASSEH ATSU',
                'created_at' => now(),
                'updated_at' => now(),
            ],
                [
                'video_link' => 'https://www.youtube.com/embed/1KIfglx7dgs',
                'video_description' => 'Destiny',
                'created_at' => now(),
                'updated_at' => now(),
           ],
                [
                    'video_link' => 'https://www.youtube.com/embed/e5hbc3zEZz0',
                    'video_description' => 'Seven (7) Stages of Spiritual Awakening and Spiritual Growth by Prophet Atsu Manasseh',
                    'created_at' => now(),
                    'updated_at' => now(),
                 ],
            ]

        );
    }
}


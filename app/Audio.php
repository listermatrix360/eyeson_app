<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Audio extends Model
{
    use SoftDeletes;
    protected $fillable
        =
        [
            'path',
            'title',
            'description',
            'thumbnail',
        ];
}

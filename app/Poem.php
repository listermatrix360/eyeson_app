<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Poem extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'content', 'image'];

    public function poemUrl(){
        return url('dashboard/poem/'.$this->id.'/'.Str::slug($this->title));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Blog extends Model
{
	protected $fillable = [
		'title',
		'user_id',
		'blog_image',
		'description'
	];
    public function postUrl()
    {
        return url('/blog/detail/'.$this->id.'/'. Str::slug($this->title));
    }

    public function posts()
    {
        return url('/catalogue/posts/'.$this->id.'/'. Str::slug($this->title));
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }
}

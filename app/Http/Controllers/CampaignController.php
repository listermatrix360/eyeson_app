<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;
use App\Newsletter;
use Illuminate\Support\Facades\DB;
class CampaignController extends Controller
{
    public function index()
    {
    	return view('campaign.index');
    }

    public function create()
    {
    	$newsletters = Newsletter::all();
    	return view('campaign.create', compact('newsletters'));
    }

    public function store(Request $request){
    	return $request->all();
    }

   
    public function view_mailist(){
    	$newsletters = Newsletter::orderBy('id', 'desc')->paginate(20);
    	return view('campaign.mailist', compact('newsletters'));
    }

    // public function loadMailist()
    // {
    // 	$output = '';
    // 	$newsletters = Newsletter::all();
    // 	foreach ($newsletters as $newsletter) {
    // 		$output .= $newsletter->email;
    // 	}
    // 	return response()->json([
    // 		'output' => $output,
    // 	]);
    // }
}

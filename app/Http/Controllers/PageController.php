<?php

namespace App\Http\Controllers;
use App\Sermons;
use App\Events;
use App\Testimonies;
use App\Branches;
use App\Member;
use App\ChurchProject;
use Illuminate\Http\Request;


class PageController extends Controller
{

    public function home(){
        return view('welcome');
    }


    public function error(){
    	return view('web_pages.errors.404');
    }


}

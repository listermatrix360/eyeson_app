<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;

class PdfController extends Controller
{


    public function index()
    {
        $videos = Document::query()->get();
        return view('videos.index',compact('videos'));
    }

    public function create(Request  $request)
    {

        if($request->isMethod('post')):

            $request->validate(
                [
                    'video_link'=>'required',
                    'video_description'=>'required',
                ]
            );

            $data = $request->all();
            Document::query()->create($data);
            return  redirect()->route('items.videos')->with('success','Video successfully added');
        endif;



        return view('videos.create');
    }

    public function edit(Request  $request,Document  $video)
    {


        if($request->isMethod('post')):

            $request->validate(
                [
                    'video_link'=>'required',
                    'video_description'=>'required',
                ]
            );

            $data = $request->all();

            $video->update($data);

            return  redirect()
                ->route('items.videos')
                ->with('success','Video successfully added');
        endif;



        return view('videos.edit',compact('video'));
    }



}

<?php

namespace App\Http\Controllers;

use App\User;
use App\Events;
use App\Groups;
use App\Staffs;
use App\Members;
use App\Contacts;
use App\Products;
use App\Offerings;
use App\Departments;
use App\Announcements;
use App\Charts\BarChart;
use App\Charts\PulseChart;
use App\Video;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('admin.home',[
            'members' => $members ?? 0,
            'staff' => $staff ?? 0,
            'an' => $an ?? 0,
            'event' => $event  ?? 0,
            'product' => $product ?? 0,
            'groups' => $groups ?? 0,
            'dept' => $dept ?? 0,
            'chart' => $chart ?? 0,
            'income' => $income ?? 0
        ]);
    }

    public function edit($id) {
        $user = User::where('id', $id)->first();

        return view('user.edit', compact('user'));
    }


    public function update(Request $request, $id)
    {
      $attributes = request()->validate([
           'avatar' => ['image'],
            'name' =>
                       [
                           'required',
                           'string',
                           'max:255'
                       ],
            'email' => ['required', 'string', 'email', 'max:255'],

        ]);

      if(!empty($request->input('password')))
          request()->validate(
              ['password' => ['required', 'min:8', 'max:255', 'confirmed']]
          );



        if($request->hasFile('avatar')) {
            $image_name = $request->hidden_image;

            $image = $request->file('avatar');
            $image_name = rand() . '.' . strtolower($image->getClientOriginalExtension());
            $image->move(public_path('ProfileImg'), $image_name);
            $formData = ['avatar' => $image_name];

       } else {
        $formData = [
            'avatar' => '',
            'name' => $attributes['name'],
            'email' => $attributes['email'],
            'password' => $attributes['password']
           ];
       }
        User::query()->find($id)->update($formData);
        return redirect(route('members'))->withSuccess('Profile Updated Successfully');
    }



    public function videos()
    {
        $videos = Video::query()->get();
        return view('videos.index',compact('videos'));
    }

    public function videos_create(Request  $request)
    {

        if($request->isMethod('post')):

            $request->validate(
            [
                'video_link'=>'required',
                'video_description'=>'required',
            ]
        );

        $data = $request->all();
        Video::query()->create($data);
        return  redirect()->route('items.videos')->with('success','Video successfully added');
        endif;



        return view('videos.create');
    }

    public function videos_edit(Request  $request,Video  $video)
    {


        if($request->isMethod('post')):

            $request->validate(
                [
                    'video_link'=>'required',
                    'video_description'=>'required',
                ]
            );

            $data = $request->all();

            $video->update($data);

            return  redirect()
                ->route('items.videos')
                ->with('success','Video successfully added');
        endif;



        return view('videos.edit',compact('video'));
    }
}

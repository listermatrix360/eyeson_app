<?php

namespace App\Http\Controllers\Admin;

use App\Artwork;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArtworkController extends Controller
{
    public function index()
    {
        return view('admin.artworks.index');
    }

    public function create()
    {
        return view('admin.artworks.create');
    }

    public function store(Request $request)
    {
        $artwork = new Artwork();
        $rules = [
            'image' => ['required', 'mimes: pdf', 'max: 5000'],
            'file' => ['required', 'image','mimes:jpeg,png,jpg,gif,svg','max:2048'],
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()){
            return back()->withErrors($validator);
        }


    }

    public function show()
    {

    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::query()->orderBy('id', 'desc')->get();
        return view('admin.article.index', compact('articles'));
    }

    public function create(){
        return view('admin.article.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => ['required'],
            'image' => ['required', 'image','mimes:jpeg,png,jpg,gif,svg','max:2048'],
            'content' => ['required'],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            return back()->withErrors($validator);
        }else{
            $image = $request->file('image');
            $article_image = time().$image->getClientOriginalName();
            $image->move('images/article_images', $article_image);
            DB::beginTransaction();
            Article::create([
                'title' => $request->title,
                'image' => 'images/article_images/'.$article_image,
                'content' => $request->content,
            ]);
            DB::commit();
            Session::flash('success', 'Article has been saved successfully');
            return back();
        }
    }

    public function show($id)
    {
        $article = Article::find($id);
        return view('admin.article.show', compact('article'));
    }
    public function edit($id)
    {
        $article = Article::find($id);
        return view('admin.article.edit', compact('article'));
    }

    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $rules = [
            'title' => ['required'],
            'content' => ['required'],
        ];
        $article->title = $request->title;
        $article->content = $request->content;
        $validator = Validator::make($request->all(), $rules);
        if ($request->image) {
            if ($article->image != null && File::exists( $article->image)) {
                File::delete($article->image);
            }

            $image = $request->file('image');
            $article_image = time().$image->getClientOriginalName();
            $image->move('images/article_images/', $article_image);
            $article->image = 'images/article_images/'.$article_image;
        }
        if ($validator->fails()){
            return back()->withErrors($validator);
        }else{
            DB::beginTransaction();
            $article->save();
            DB::commit();
            Session::flash('success', 'Article has been updated successfully');
            return back();
        }
    }

    public function delete($id)
    {
        $article = Article::find($id);
        $image = public_path($article->image);
        if (File::exists($image)) {
            File::delete($image);
        }
        $article->delete();
        Session::flash('success', 'Article has been deleted successfully');
        return back();
    }
}

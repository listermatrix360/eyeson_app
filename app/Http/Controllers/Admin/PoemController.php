<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PoemsDataTable;
use App\Http\Controllers\Controller;
use App\Poem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class PoemController extends Controller
{
    public function index()
    {
        $poems = Poem::query()->orderBy('id', 'desc')->get();
        return view('admin.poems.index', compact('poems'));
    }

    public function create()
    {
        return view('admin.poems.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => ['required'],
            'image' => ['required', 'image','mimes:jpeg,png,jpg,gif,svg','max:2048'],
            'content' => ['required'],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            return back()->withErrors($validator);
        }else{
            $image = $request->file('image');
            $poem_image = time().$image->getClientOriginalName();
            $image->move('images/poem_images', $poem_image);
            DB::beginTransaction();
            Poem::create([
                'title' => $request->title,
                'image' => 'images/poem_images/'.$poem_image,
                'content' => $request->content,
            ]);
            DB::commit();
            Session::flash('success', 'Poem has been saved successfully');
            return back();
        }
    }

    public function show($id)
    {
        $poem = Poem::find($id);
        return view('admin.poems.show', compact('poem'));
    }

    public function edit($id)
    {
        $poem = Poem::find($id);
        return view('admin.poems.edit', compact('poem'));
    }

    public function update(Request $request, $id)
    {
        $poem = Poem::find($id);
        $rules = [
            'title' => ['required'],
            'content' => ['required'],
        ];
        $poem->title = $request->title;
        $poem->content = $request->content;
        $validator = Validator::make($request->all(), $rules);
//        $image = public_path($poem->image);
        if ($request->image) {
            if ($poem->image != null && File::exists( $poem->image)) {
                File::delete($poem->image);
            }

            $image = $request->file('image');
            $poem_image = time().$image->getClientOriginalName();
            $image->move('images/poem_images/', $poem_image);
            $poem->image = 'images/poem_images/'.$poem_image;
//            return $poem->image;
        }
        if ($validator->fails()){
            return back()->withErrors($validator);
        }else{
            DB::beginTransaction();
            $poem->save();
            DB::commit();
            Session::flash('success', 'Poem has been updated successfully');
            return back();
        }
    }

    public function delete($id)
    {
        $poem = Poem::find($id);
        $image = public_path($poem->image);
        if (File::exists($image)) {
            File::delete($image);
        }
        $poem->delete();
        Session::flash('success', 'Poem has been deleted successfully');
        return back();
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
        {
            //this is a custom redirection when users visit secured pages without authentication

            if ($guard == "user" && Auth::guard($guard)->check()) {
                return redirect('login');
            }
            if ($guard == "member" && Auth::guard($guard)->check()) {
                return redirect('subscriber/index');
            }
            if (Auth::guard($guard)->check()) {
                return redirect('/dashboard');
            }

            return $next($request);
        }
}

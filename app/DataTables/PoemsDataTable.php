<?php

namespace App\DataTables;

use App\Poem;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PoemsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function($query){
                return Carbon::parse($query->created_at);
            });
//            ->addColumn('action', 'poems.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\App\Poem $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Poem $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('poems-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                     ->buttons(
                        Button::make('create'),
                        Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
//            Column::computed('action')
//                ->exportable(true)
//                ->printable(false)
//                ->width(60)
//                ->addClass('text-center'),
            Column::make('title')->title('Title'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Poems_' . date('YmdHis');
    }
}

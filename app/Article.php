<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Article extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'content', 'image'];

    public function articleUrl(){
        return url('dashboard/article/'.$this->id.'/'.Str::slug($this->title));
    }
}
